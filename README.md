# Atlassian Connect Express Mongo Adapter

This is a basic MongoDB adapter for Atlassian Connect express. It was created due to the deprecation of JugglingDB adapter for Mongodb in versions 3.X.X.

To use this adapter, add the following dependency in your `package.json`:

    "atlassian-connect-express-mongo": "~0.0.0"

Then, in your `config.json`, specify your datastore:

    "store": {
      "adapter": "mongo",
      "url": "mongodb://localhost:27017"
    }

You'll also need to register the adapter in your `app.js`:

    ac.store.register('mongo', require('atlassian-connect-express-mongo'));

And that's it.

## Bugs?

To report a file please click [here](https://bitbucket.org/xpandit/atlassian-connect-express-mongo/issues).
To contribute, please raise a [PR](https://bitbucket.org/xpandit/atlassian-connect-express-mongo/pull-requests/).
