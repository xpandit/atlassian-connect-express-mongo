const mongoose = require('mongoose');
const bluebird = require('bluebird');

let connectionPromise;

function getAsObject(val) {
    if (typeof val === 'string') {
        try {
            val = JSON.parse(val);
        } catch (e) {
        // it's OK if we can't parse this. We'll just return the string below.
        }
    }

    return val;
}

function connect(url) {
    // Connection ready state
    // 0 = disconnected
    // 1 = connected
    // 2 = connecting
    // 3 = disconnecting
    const connected = mongoose.connection.readyState === 1 ||
        mongoose.connection.readyState === 2;

    console.log('Connection Status:', connected);

    if (!connected) {
        mongoose.connection.on('connected', () => {
            console.log('Mongoose connection open');
        });

        // If the connection throws an error
        mongoose.connection.on('error', err => {
            console.log(`Mongoose connection error: ${err}`);
        });

        // When the connection is disconnected
        mongoose.connection.on('disconnected', () => {
            console.log('Mongoose connection disconnected');
        });

        return mongoose.connect(url);
    }

    return bluebird.resolve();
}

function MongoAdapter(logger, opts) {
    opts = opts || {
        url: 'mongodb://localhost:27017'
    };

    let configUrl = opts.url || opts.rs;

    if (configUrl === '$MONGODB_URI')
        configUrl = process.env.DATABASE_URL;

    const settingsSchema = new mongoose.Schema({
        _id: String,
        clientKey: String,
        key: String,
        val: Object
    });

    settingsSchema.index({ clientKey: 1, key: 1 });

    const addonSettings = mongoose.model(
        'AddonSettings', settingsSchema, 'AddonSettings'
    );

    connectionPromise = connect(configUrl).then(() => addonSettings);
}

const proto = MongoAdapter.prototype;

// Get the AddonSettings object with 'key' belonging to tenant 'clientKey'
proto.get = (key, clientKey) =>
    connectionPromise.then(AddonSettings =>
        AddonSettings.findOne({ key, clientKey })).then(data => {
        if (data)
            return getAsObject(data.val);
        return {};
    });


// Delete the AddonSettings object with 'key' belonging to tenant 'clientKey'
proto.del = (key, clientKey) =>
    connectionPromise.then(AddonSettings =>
        AddonSettings.deleteOne({ key, clientKey }));

// Store/Update the AddonSettings object with 'key' belonging to tenant
// 'clientKey'
proto.set = (key, value, clientKey) =>
    connectionPromise.then(AddonSettings =>
        AddonSettings.findOneAndUpdate(
            { key, clientKey },
            { val: value },
            { upsert: true }
        ));

// Get all ClientInfos from database
proto.getAllClientInfos = () =>
    connectionPromise.then(AddonSettings => {
        AddonSettings.find({ key: 'clientInfo' });
    }).then(data => {
        if (!Array.isArray(data))
            return [];
        return data.map(elem => getAsObject(elem.val));
    });

proto.isMemoryStore = () => false;

module.exports = (logger, opts) => new MongoAdapter(logger, opts);
